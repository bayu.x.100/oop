<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    require ('./animal.php');
    require ('./ape.php');
    require ('./frog.php');

    $sheep = new Animal ("shaun");

    echo $sheep->name; // "shaun"
    echo "<br>";
    echo $sheep->legs; // 2
    echo "<br>";
    echo $sheep->cold_blooded; // false 
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo $sungokong->name;
    echo "<br>";
    echo $sungokong->legs;
    echo "<br>";  
    $sungokong->yell(); // "Auooo" 
    echo "<br><br>";  

    $kodok = new Frog("buduk");
    echo $kodok->name;
    echo "<br>";
    echo $kodok->legs;
    echo "<br>";
    $kodok->jump(); // "hop hop"
    echo "<br><br>";  

    ?>
</body>
</html>